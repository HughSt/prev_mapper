library(shiny)
library(leaflet)
library(raster)
library(rgdal)
library(spatstat)
library(oro.nifti)
library(mgcv)
library(rgeos)
library(sp)
library(rworldmap)
library(shinyBS)
source("auto_prev_functions.R")

# Get map
map = leaflet() %>% addTiles('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
                             attribution = paste(
                               '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
                             )) 

shinyServer(function(input, output){

  output$raster_map = renderLeaflet({
    
    inFile<-input$File
    if (is.null(inFile))
     return(NULL)
    points<-read.csv(inFile$datapath)
    
    # Give loading bar
    withProgress(message = 'Hold on',
                 detail = 'Crunching data..',
                 value = 5,
                 {
    
    # load input data
    input_data <- prepare_input_data(points)
    
    # prepare data for model
    #if(country %in% ccodes()$ISO3[ccodes()$CONTINENT=="Africa"]){
    model_data <- prepare_data_for_model_fast(input_data)
    #}else{
    #  model_data <- prepare_data_for_model(input_data)
    #}
    
    # Run model
    result <- run_model(model_data)
    
    # Make predictions
    #country_prediction <- make_predictions_for_country(result, model_data, input_data)
    #cropped_prediction <- make_predictions_for_country(result, model_data, input_data)
    
    # Sim from posterior
    sim_from_posterior_predictions <<- simulate_from_posterior(result, model_data, input_data)
    sim_from_posterior_predictions$pred_raster[sim_from_posterior_predictions$pred_pixels] <- apply(sim_from_posterior_predictions$fits_prev, 1, 
                                                          function(x) {mean(x>=0 & x<=0.5)}) # hard coded to avoid rerunning every time input$prevalence chagnes
    
    # Make predicitons at locations with no data
    prediction_class <- predict_prev_class(input_data, sim_from_posterior_predictions$best_estimate)

    # define modal table
    output_table <- subset(prediction_class, select = -c(lng, lat, Nex, Npos))
    output$pred_table <- renderDataTable({output_table})
    
    ## MAP
    #map_data (cropped_prediction, model_data, prediction_class, pal)
    map_sim_data(sim_from_posterior_predictions, model_data, prediction_class)
    })
  
  })
  
  # Filter by exceednace probability
  observeEvent(input$go, {
    
      withProgress(message = 'Recalculating',
                   detail = 'This may take a while...', value = 0, {
    
    prev_threshold <- input$prevalence
    pop_threshold <- input$pop   
    
    # recalcuate exceedance probabilities
    sim_from_posterior_predictions$pred_raster[sim_from_posterior_predictions$pred_pixels] <- apply(sim_from_posterior_predictions$fits_prev, 1, 
                                                                                                    function(x) {mean(x>=input$prevalence[1] & x<=input$prevalence[2])})
    
    # Filter by pop
    sim_from_posterior_predictions$pred_raster[which(bio_stack_agg[[23]][]<pop_threshold)] <- NA
    
    # Remap
    pal <-colorNumeric(tim.colors(n=64),seq(0,1,0.01),
                       na.color = "transparent")
    
                     leafletProxy("raster_map") %>%
                       clearImages() %>%
                       addRasterImage(sim_from_posterior_predictions$pred_raster,
                                      col = pal, opacity = 0.7, group = "Probability", project=FALSE) %>%
                       
                       addRasterImage(sim_from_posterior_predictions$best_estimate, opacity = 0.7, col=pal, group = "Best estimate")
                   })
  })
    
  
  ### Cropping by mean
  # observeEvent(input$go, {
  # 
  #   withProgress(message = 'Filtering',
  #                detail = 'This may take a while...', value = 0, {
  # 
  # 
  #                  prev_threshold <- input$prevalence
  #                  pop_threshold <- input$pop
  # 
  #                  new_prev_map <- cropped_prediction
  #                 
  #                  new_prev_map[unique(c(which(new_prev_map[[1]][]<prev_threshold[1]),
  #                                      which(new_prev_map[[1]][]>prev_threshold[2]),
  #                                      which(bio_stack_agg[[23]][]<pop_threshold)))] <- NA
  #                  
  #                  # Define color palette
  #                  pal <-colorNumeric(tim.colors(n=64),values(cropped_prediction[[1]]),
  #                                     na.color = "transparent")
  #                  
  #                  pal_se <- colorNumeric(brewer.pal(9,"Greens"),values(cropped_prediction[[2]]),
  #                                         na.color = "transparent")
  #                  
  #                  # Remap
  #                  leafletProxy("raster_map") %>%
  #                    clearImages() %>%
  #                    addRasterImage(new_prev_map[[1]],
  #                                   col = pal, opacity = 0.7, group = "Prevalence map") %>%
  #                    
  #                    addRasterImage(new_prev_map[[2]], 
  #                                   colors=pal_se,
  #                                   opacity=0.7, group = "Prediction SE") 
  # 
  #                })
  # 
  # })
  
  
  #output$pred_table <- renderDataTable({prediction_table})
  output$downloadData <- downloadHandler(
    filename = function() {
      paste('risk', Sys.Date(), '.tif', sep='')
    },
    content = function(con) {
      writeRaster(sim_from_posterior_predictions$best_estimate, con)
    }
  )
  
  output$logo <- renderImage({
    
    # Return a list containing the filename
    list(src = "logo_transparent.png")
  }, deleteFile = FALSE)
  
  output$EE_logo <- renderImage({
    
    # Return a list containing the filename
    list(src = "GoogleEarthEngine_logo.png")
  }, deleteFile = FALSE)
  
})  



